package com.sunat.practica.gradle.model;

public class Recibo {
	
	private Integer id;
	private Integer id_empresa;
	private String nro_recibo;
	private Double monto_emitido;	
	private Integer  fg_retencion;
	
	public Recibo() {
		// TODO Auto-generated constructor stub
	}
	
	public Recibo(Integer id,
	 Integer id_empresa,
	 String nro_recibo,
	 Double monto_emitido,
	 Integer  fg_retencion) {
		this.id = id;
		this.id_empresa = id_empresa;
		this.nro_recibo = nro_recibo;
		this.monto_emitido = monto_emitido;
		this.fg_retencion = fg_retencion;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getId_empresa() {
		return id_empresa;
	}
	public void setId_empresa(Integer id_empresa) {
		this.id_empresa = id_empresa;
	}
	public String getNro_recibo() {
		return nro_recibo;
	}
	public void setNro_recibo(String nro_recibo) {
		this.nro_recibo = nro_recibo;
	}
	public Double getMonto_emitido() {
		return monto_emitido;
	}
	public void setMonto_emitido(Double monto_emitido) {
		this.monto_emitido = monto_emitido;
	}
	public Integer getFg_retencion() {
		return fg_retencion;
	}
	public void setFg_retencion(Integer fg_retencion) {
		this.fg_retencion = fg_retencion;
	}
	
	


}
