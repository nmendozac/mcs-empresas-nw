package com.sunat.practica.gradle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunat.practica.gradle.bean.ResponseBean;
import com.sunat.practica.gradle.model.Empresa;
import com.sunat.practica.gradle.service.impl.EmpresaServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/empresa")
@Api(description = "API para el mantenimiento de los datos de empresa")
public class EmpresaController {
	
	@Autowired
	private EmpresaServiceImpl _empresaService;
	
	@GetMapping( produces = "application/json")	
	@ApiOperation("Listado General")
	public List<Empresa> getAllEmpresas(){
		return _empresaService.getAllEmpresas();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")	
	public Empresa getEmpresa(@PathVariable ("id") Integer id){
		return _empresaService.getEmpresa(id);
	}
	
	@PostMapping(produces = "application/json")	
	public ResponseBean saveEmpresa(@RequestBody Empresa empresa){
		
		_empresaService.saveEmpresa(empresa);
		
		ResponseBean bean = new ResponseBean();
		bean.setCodigo_servicio("0000");
		bean.setDescripcion("Empresa registrada con exito");
		
		return bean;
	}	


}
