package com.sunat.practica.gradle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sunat.practica.gradle.bean.ResponseBean;
import com.sunat.practica.gradle.model.Recibo;
import com.sunat.practica.gradle.service.impl.ReciboServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/recibo")
@Api(description = "API para el mantenimiento de los datos de recibo")
public class ReciboController {
	
	@Autowired
	private ReciboServiceImpl _reciboService;
	
	@PostMapping(produces = "application/json")	
	public ResponseBean saveRecibo(@RequestBody Recibo recibo){
		
		_reciboService.saveRecibo(recibo);
		
		ResponseBean bean = new ResponseBean();
		bean.setCodigo_servicio("0000");
		bean.setDescripcion("Recibo registrado con exito");
		
		return bean;
	}	


}
