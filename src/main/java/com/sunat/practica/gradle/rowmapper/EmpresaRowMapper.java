package com.sunat.practica.gradle.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sunat.practica.gradle.model.Empresa;

public class EmpresaRowMapper implements RowMapper<Empresa>{

	@Override
	public Empresa mapRow(ResultSet rs, int rowNum) throws SQLException {
		Empresa empresa = new Empresa();
		
		empresa.setId(rs.getInt("id"));
		empresa.setId_empresa(rs.getInt("id_empresa"));
		empresa.setRuc(rs.getString("ruc"));
		empresa.setRazon_social(rs.getString("razon_social"));
		empresa.setEstado_actual(rs.getString("estado_actual"));


		
		return empresa;
	}

}
