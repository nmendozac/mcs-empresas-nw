package com.sunat.practica.gradle.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.sunat.practica.gradle.model.Empresa;
import com.sunat.practica.gradle.model.Recibo;

public class ReciboRowMapper implements RowMapper<Recibo>{

	@Override
	public Recibo mapRow(ResultSet rs, int rowNum) throws SQLException {
		Recibo recibo = new Recibo();
		
		recibo.setId(rs.getInt("id"));
		recibo.setId_empresa(rs.getInt("id_empresa"));
		recibo.setMonto_emitido(rs.getDouble("monto_emitido"));
		recibo.setNro_recibo(rs.getString("nro_recibo"));
		recibo.setFg_retencion(rs.getInt("fg_retencion"));
		
		return recibo;
	}

}
