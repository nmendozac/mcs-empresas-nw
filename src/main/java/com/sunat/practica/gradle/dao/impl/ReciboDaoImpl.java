package com.sunat.practica.gradle.dao.impl;

import java.sql.Types;

import javax.sql.DataSource;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.sunat.practica.gradle.dao.ReciboDao;
import com.sunat.practica.gradle.model.Recibo;
@Repository
public class ReciboDaoImpl extends JdbcDaoSupport implements ReciboDao {
	
	public ReciboDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}

	@Override
	public void saveRecibo(Recibo recibo) {
		// TODO Auto-generated method stub
		String sql = "insert into microservicios.recibo (id_empresa, nro_recibo, monto_emitido, fg_retencion) "  
				+ "values (?, ?, ?, ?);";
		
		Object[] params = { recibo.getId_empresa(), recibo.getNro_recibo(), recibo.getMonto_emitido(), recibo.getFg_retencion()};
		int[] tipos = { Types.INTEGER, Types.VARCHAR, Types.DOUBLE, Types.VARCHAR};
		
		try {
			
			int filas = getJdbcTemplate().update(sql, params,tipos);
			
			logger.debug("Se han insertado : "+filas+" filas");
			logger.debug("Se ha registrado a la empresa "+recibo.toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

}
