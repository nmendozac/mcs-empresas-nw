package com.sunat.practica.gradle.dao;

import com.sunat.practica.gradle.model.Recibo;

public interface ReciboDao {
	public void saveRecibo(Recibo recibo);
}
