package com.sunat.practica.gradle.dao;

import java.util.List;

import com.sunat.practica.gradle.model.Empresa;

public interface EmpresaDao {
	public List<Empresa> getAllEmpresas();
	public Empresa getEmpresa(Integer id);
	public void saveEmpresa(Empresa empresa);
	
}
