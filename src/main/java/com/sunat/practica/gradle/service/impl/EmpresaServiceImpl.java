package com.sunat.practica.gradle.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunat.practica.gradle.dao.impl.EmpresaDaoImpl;
import com.sunat.practica.gradle.model.Empresa;
import com.sunat.practica.gradle.service.EmpresaService;

@Service
public class EmpresaServiceImpl implements EmpresaService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmpresaDaoImpl _empresaDao;
	
	@Override
	public List<Empresa> getAllEmpresas() {
		
		return _empresaDao.getAllEmpresas();
	}

	@Override
	public Empresa getEmpresa(Integer id) {
		
		return _empresaDao.getEmpresa(id);
	}

	@Override
	public void saveEmpresa(Empresa persona) {
		try {
			_empresaDao.saveEmpresa(persona);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
	}



}
