package com.sunat.practica.gradle.service;

import java.util.List;

import com.sunat.practica.gradle.model.Empresa;

public interface EmpresaService {
	
	List<Empresa> getAllEmpresas();
	Empresa getEmpresa(Integer id);
	void saveEmpresa(Empresa empresa);
	
}
