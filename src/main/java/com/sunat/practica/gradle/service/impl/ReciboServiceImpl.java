package com.sunat.practica.gradle.service.impl;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sunat.practica.gradle.dao.impl.ReciboDaoImpl;
import com.sunat.practica.gradle.model.Recibo;
import com.sunat.practica.gradle.service.ReciboService;

@Service
public class ReciboServiceImpl implements ReciboService{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ReciboDaoImpl _reciboDao;
	
	@Override
	public void saveRecibo(Recibo recibo) {
		try {
			_reciboDao.saveRecibo(recibo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}	
	}



}
