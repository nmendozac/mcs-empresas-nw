package com.sunat.practica.gradle.service;

import com.sunat.practica.gradle.model.Recibo;

public interface ReciboService {
	
	void saveRecibo(Recibo recibo);
	
}
